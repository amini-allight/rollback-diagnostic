// This part is public domain (CC0 1.0)
#pragma once

#include <cstdint>
#include <cstring>
#include <cstdio>
#include <vector>

#pragma pack(1)
struct RollbackDiagnosticEntry
{
    int64_t index;
    double value;
    char text[16];
};
#pragma pack()

class RollbackDiagnostic
{
public:
    RollbackDiagnostic()
    {

    }

    ~RollbackDiagnostic()
    {
        FILE* file = std::fopen("rollback.diag", "wb");

        std::fwrite(entries.data(), sizeof(RollbackDiagnosticEntry), entries.size(), file);

        std::fclose(file);
    }

    void record(int64_t index, double value, const char* text)
    {
        RollbackDiagnosticEntry entry{};
        entry.index = index;
        entry.value = value;
        std::memcpy(entry.text, text, std::strlen(text) < 15 ? std::strlen(text) : 15);

        entries.push_back(entry);
    }

private:
    std::vector<RollbackDiagnosticEntry> entries;
};
