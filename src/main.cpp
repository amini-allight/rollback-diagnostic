/*
Copyright 2022 Amini Allight

This file is part of Rollback Diagnostic.

Rollback Diagnostic is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Rollback Diagnostic is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Rollback Diagnostic. If not, see <https://www.gnu.org/licenses/>.
*/
#include <QtWidgets/QtWidgets>

#include <cstdint>
#include <set>
#include <map>
#include <streambuf>
#include <fstream>
#include <iostream>

using namespace std;

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef int8_t i8;
typedef int16_t i16;
typedef int32_t i32;
typedef int64_t i64;

typedef float f32;
typedef double f64;

enum class EntryType
{
    Normal,
    Correction,
    Redo
};

#pragma pack(1)
struct Entry
{
    i64 index;
    f64 value;
    char text[16];
};
#pragma pack()

static const QColor normalColor(38, 162, 105);
static const QColor correctionColor(237, 51, 59);
static const QColor replayColor(24, 103, 66);

static const i32 entryBorderWidth = 4;
static const i32 entryMarginWidth = 16;
static const QSize entrySize(128, 64);
static const QSize entryTotalSize = entrySize + QSize(entryMarginWidth, entryMarginWidth) * 2;

static vector<Entry> entries;

QPoint operator*(const QPoint& p, const QSize& q)
{
    return {
        p.x() * q.width(),
        p.y() * q.height()
    };
}

class Window : public QWidget
{
public:
    Window()
        : QWidget()
    {
        setWindowTitle("Rollback Diagnostic");
        setGeometry(0, 0, 1600, 900);
        setMouseTracking(true);
    }

private:
    QPoint cameraOffset;
    bool cameraGrip = false;
    QPoint cameraGripPrevious;

    void paintEvent(QPaintEvent* event) override
    {
        QPainter painter(this);

        painter.setRenderHint(QPainter::Antialiasing);

        painter.fillRect(0, 0, width(), height(), Qt::black);

        map<i64, f64> previousValues;
        i64 previousIndex = -1;
        QPoint previousPoint(0, -1);
        QPoint point;

        for (const Entry& entry : entries)
        {
            EntryType type;

            i64 expectedIndex = previousIndex + 1;

            if (entry.index != expectedIndex)
            {
                point += QPoint(1, -(expectedIndex - entry.index));
                type = EntryType::Correction;
            }
            else if (previousValues.contains(entry.index))
            {
                type = EntryType::Redo;
            }
            else
            {
                type = EntryType::Normal;
            }

            drawEntry(painter, previousValues, previousPoint, point, type, entry);

            previousValues.insert_or_assign(entry.index, entry.value);
            previousIndex = entry.index;
            previousPoint = point;
            point += QPoint(0, 1);
        }
    }

    void mouseMoveEvent(QMouseEvent* event) override
    {
        if (cameraGrip)
        {
            cameraOffset += cameraGripPrevious - event->pos();

            if (cameraOffset.x() < 0)
            {
                cameraOffset.setX(0);
            }

            if (cameraOffset.y() < 0)
            {
                cameraOffset.setY(0);
            }

            update();
        }

        cameraGripPrevious = event->pos();
    }

    void mousePressEvent(QMouseEvent* event) override
    {
        switch (event->button())
        {
        case Qt::MiddleButton :
            cameraGrip = true;
            break;
        default :
            QWidget::mousePressEvent(event);
            break;
        }
    }

    void mouseReleaseEvent(QMouseEvent* event) override
    {
        switch (event->button())
        {
        case Qt::MiddleButton :
            cameraGrip = false;
            break;
        default :
            QWidget::mouseReleaseEvent(event);
            break;
        }
    }


    void drawEntry(
        QPainter& painter,
        const map<i64, f64>& previousValues,
        const QPoint& previousPoint,
        const QPoint& point,
        EntryType type,
        const Entry& entry
    ) const
    {
        QColor color;

        switch (type)
        {
        case EntryType::Normal :
            color = normalColor;
            break;
        case EntryType::Correction :
        case EntryType::Redo :
            color = entry.value == previousValues.at(entry.index) ? replayColor : correctionColor;
            break;
        }

        bool correction = type == EntryType::Correction;

        QPen pen;
        pen.setWidth(entryBorderWidth);
        pen.setColor(color);
        painter.setPen(pen);

        QFont font;
        font.setBold(true);
        painter.setFont(font);

        if (correction)
        {
            QPoint exit = toExitPoint(previousPoint, true);
            QPoint enter = toEnterPoint(point, true);

            QPainterPath path(exit);
            path.cubicTo(exit + QPoint(entryMarginWidth, 0), enter - QPoint(entryMarginWidth, 0), enter);

            painter.drawPath(path);
        }
        else
        {
            painter.drawLine(
                toExitPoint(previousPoint, false),
                toEnterPoint(point, false)
            );
        }

        QRect rect(toScreen(point), entrySize);
        QRect topRect(
            toScreen(point) + QPoint(0, entryBorderWidth),
            QSize(entrySize.width(), (entrySize.height() - (entryBorderWidth * 2)) / 2)
        );
        QRect botRect(
            toScreen(point) + QPoint(0, entrySize.height() / 2),
            QSize(entrySize.width(), (entrySize.height() - (entryBorderWidth * 2)) / 2)
        );

        painter.drawRect(rect);
        painter.drawText(topRect, Qt::AlignCenter, (to_string(entry.index) + " " + to_string(entry.value)).c_str());
        painter.drawText(botRect, Qt::AlignCenter, entry.text);
    }

    QPoint toExitPoint(const QPoint& p, bool correction) const
    {
        if (correction)
        {
            return toScreen(p) + QPoint(entrySize.width() + entryBorderWidth, entrySize.height() / 2);
        }
        else
        {
            return toScreen(p) + QPoint(entrySize.width() / 2, entrySize.height() + entryBorderWidth);
        }
    }

    QPoint toEnterPoint(const QPoint& p, bool correction) const
    {
        if (correction)
        {
            return toScreen(p) + QPoint(-entryBorderWidth, entrySize.height() / 2);
        }
        else
        {
            return toScreen(p) + QPoint(entrySize.width() / 2, 0);
        }
    }

    QPoint toScreen(const QPoint& p) const
    {
        return ((p * entryTotalSize) + QPoint(entryMarginWidth, entryMarginWidth)) - cameraOffset;
    }
};

int main(int argc, char** argv)
{
    if (argc != 2)
    {
        cout << "Usage: rollback-diagnostic <file>" << endl;
        return 1;
    }

    ifstream file(argv[1]);
    string data = string(istreambuf_iterator<char>(file), istreambuf_iterator<char>());

    entries = vector<Entry>(data.size() / sizeof(Entry));
    memcpy(entries.data(), data.data(), data.size());

    QApplication app(argc, argv);

    auto window = new Window();

    window->show();

    return app.exec();
}
