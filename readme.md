# Rollback Diagnostic

A tool for creating and visualizing diagnostic dumps of rollback networking behavior.

## Usage

Insert the provided code snippet `plugin.hpp` into your program and use it to report frames from your networked main loop. Frames have an incrementing frame number, a value you are tracking (for example part of an object's position) and an optional 15 character text field which can be used to report additional diagnostic data. Run the program to generate a file called `rollback.diag` and then open it with the following command:

```
./rollback-diagnostic /path/to/rollback.diag
```

To view the visualization, which looks like this:

![](doc/demo.png)

The visualization can be panned using middle mouse click and drag. Time advances from the top of the screen to the bottom, while alternate "timelines" created by rollback networking are displayed left-to-right. Bright green indicates the first time a frame has been recorded. Dim green indicates a frame has been recomputed with a value that matched the previous computation, while red indicates a frame has been recomputed with a value that differs from the previous computation, likely resulting in a visible misprediction error for the user.

## Dependencies

- Uses Qt5 to render the GUI.

## License

Developed by Amini Allight. Licensed under the GPL 3.0, except the plugin, which is public domain (CC0 1.0).
